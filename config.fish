set PATH $HOME/.cargo/bin $PATH

# DB
alias _ffenvreset "docker-compose exec  --user 1000:1000 php php bin/console fitfox:dev:reset:env --no-interaction"

# Linting and Testing
alias _fflintnode "docker-compose exec  --user 1000:1000 fitfox_frontend_node npm run lint:errors"
alias _ffchecknode "_fflintnode && docker-compose exec  --user 1000:1000 fitfox_frontend_node npm test-watch-inband"
alias _fflintphp "docker-compose exec --user 1000:1000 php composer anal" 
alias _ffcheckphp "docker-compose exec --user 1000:1000 php composer check" 
alias _fflintnodefix "docker-compose exec  --user 1000:1000 fitfox_frontend_node npm run lint:fixAll"
alias _fftestnode "docker-compose exec  --user 1000:1000 fitfox_frontend_node npm run test-watch-inband -- --maxWorkers=50%"
alias _fftestnodeservices "docker-compose exec  --user 1000:1000 fitfox_frontend_node npm run test-watch-services"
alias _fftestnodeui "docker-compose exec  --user 1000:1000 fitfox_frontend_node npm run test-watch-ui"
alias _fftestphp "docker-compose exec  --user 1000:1000 php composer test"
alias _ffmdiff "docker-compose exec  --user 1000:1000 php bin/console doctrine:migrations:diff"

# Building
alias _ffnodebuilddev "docker-compose exec --user 0:0 fitfox_frontend_node chown -R 1000:1000 ./ && npm run build-development"


# Debug
alias _ffphpdebugaw "docker-compose exec --user 1000:1000 php bin/console debug:autowiring" 
alias _ffphpdebugrouter "docker-compose exec --user 1000:1000 php bin/console debug:router" 

# Install Dependencies
alias _ffnodedeps "docker-compose exec fitfox_frontend_node npm ci"
alias _ffdeadnodedeps "docker-compose run fitfox_frontend_node npm ci"

# Access Containers
alias _ffnodebash "docker-compose exec  --user 1000:1000 fitfox_frontend_node bash"

# Service/container control
alias _dup "docker-compose up -d"
alias _ddown "docker-compose down"
alias _dr "docker-compose restart"
alias _ffrnode "docker-compose restart fitfox_frontend_node"
alias _ffrproxy "docker-compose restart fitfox_web_proxy"

# Navigation
alias _cdfitfox "cd ~/Schreibtisch/code/fitfox.de/"
alias _fishconfig "code ~/.config/fish/config.fish"
alias _a "git add ."
alias s "git status"

# System
alias _update "sudo apt update && sudo apt upgrade -y"


# ---------- FUNCTIONS ------------------------------------------->

# Install a provided NPM package in node docker environment
# @Usage:  _ffnpmi lodash
function _ffnpmi --description 'npm install provided package' --argument-names packageName
        docker-compose exec fitfox_frontend_node npm i $packageName;
end

# Update Fish config
function _updateLocalFishConfig --description 'Update Fish Config File'
        git clone git@gitlab.com:evan24/fishshortcuts.git ~/fishshortcuts
        cd ~/fishshortcuts
        
        cp ./config.fish ~/.config/fish/config.fish
        cd ..
        rm -rf ~/fishshortcuts
        echo 'Removed FishConfig Repository'
end

# Update Fish config
function _pushNewFishConfig --description 'Update Fish Config File'
        git clone git@gitlab.com:evan24/fishshortcuts.git ~/fishshortcuts
        cd ~/fishshortcuts
        
        cp ~/.config/fish/config.fish ./config.fish 
        cat ~/.config/fish/config.fish
        git add .
        git commit -m 'Updated Fish Config With Script'
        git push
        cd ..
        rm -rf ~/fishshortcuts
        echo 'Removed FishConfig Repository'
end

# Run PHP test unit with a group
function _ffphptestgroup --description 'Run PHP test unit with a group' --argument-names groupName
        docker-compose exec --user 1000:1000 php composer test -- --group $groupName;
end

# Runs a command in the php container
function _execphp --description 'Run PHP test unit with a group' --argument-names cmd
        docker-compose exec --user 1000:1000 php $cmd;
end


function _ffgettoken --description 'Get Fitfox Token for organiser@fitfox.de'
        curl -X POST http://localhost/api/auth/login -d '{"email": "organiser@fitfox.de", "password": "test"}'
end
